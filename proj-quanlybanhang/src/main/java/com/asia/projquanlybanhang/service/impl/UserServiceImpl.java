package com.asia.projquanlybanhang.service.impl;

import com.asia.projquanlybanhang.entity.User;
import com.asia.projquanlybanhang.repository.UserRepository;
import com.asia.projquanlybanhang.service.UserService;
import com.asia.projquanlybanhang.util.CustomUserDetails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {

        User user = userRepository.findUserByName(userName);

        if (user == null){
            throw new UsernameNotFoundException(userName);
        }
        return new CustomUserDetails(user);
    }
}
