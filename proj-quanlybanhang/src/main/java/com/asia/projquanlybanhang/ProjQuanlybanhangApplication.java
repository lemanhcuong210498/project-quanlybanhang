package com.asia.projquanlybanhang;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import javax.sql.DataSource;

@SpringBootApplication
public class ProjQuanlybanhangApplication {

    public static void main(String[] args) {
        SpringApplication.run(ProjQuanlybanhangApplication.class, args);
    }

}
